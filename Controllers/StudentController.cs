﻿using Microsoft.AspNetCore.Mvc;
using CoursesSec001.Data;

using System.Collections.Generic;
using System.Linq;

using CoursesSec001.Models;
using CoursesSec001.Models.DataModels;


namespace CoursesSec001.Controllers
{
    public class StudentController : Controller
    {
        private readonly CoursesContext _context;

        public StudentController(CoursesContext context)
        {
            this._context = context;
        }
       
        public IActionResult Index()
        {
            //obtener todos los estudiantes
            //var students = _context.Students.ToList();
            List<Student> students = _context.Students.ToList();

            //obtienes los estudiantes que tengan el nombre juan
            /*var studentss = from student in _context.Students
                            where student.FirstName.Equals("Juan")
                            select new { student.FirstName, student.LastName };

            List<Student> students_context.Students.Where(s => s.FirstName.Equals("Juan"))
                .Select(s => s.FirstName).ToList();*/

            StudentIndexDataModel studentIndexDataModel = new StudentIndexDataModel();
            studentIndexDataModel.Students = students;

            return View(studentIndexDataModel);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Student student)
        {
            if(ModelState.IsValid)
            {
                student.EnrollmentDate = System.DateTime.Now;
                _context.Students.Add(student);
                _context.SaveChanges();   
            }
            return RedirectToAction(nameof(Index));
        }
    
        public IActionResult Edit(int studentId)
		{
            Student student = _context.Students.FirstOrDefault(s => s.ID == studentId);

            if (student == null) return NotFound();

            return View(student);
		}

		[HttpPost]
        public IActionResult Edit(Student student)
		{
			if (ModelState.IsValid)
			{
                _context.Students.Update(student);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }

            return View(student);
		}
    }
}
