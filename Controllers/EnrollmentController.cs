﻿using Microsoft.AspNetCore.Mvc;
using CoursesSec001.Models;
using CoursesSec001.Models.DataModels;

using CoursesSec001.Data;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace CoursesSec001.Controllers
{
    public class EnrollmentController : Controller
    {
        private readonly CoursesContext _context;

        public EnrollmentController(CoursesContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            List<Enrollment> enrollments = _context.Enrollments
                                                    .Include(e=> e.Course)
                                                    .Include(e => e.Student)
                                                    .ToList();

            EnrollmentsIndexDataModel model = new EnrollmentsIndexDataModel();

            model.Enrollments = enrollments;

            return View(model);
        }

        public IActionResult Create(int studentId)
        {
            Student student = _context.Students.FirstOrDefault(s => s.ID == studentId);

            if (student == null)
			{
                return NotFound();
			}

            List<Course> courses = _context.Courses.ToList();

            Enrollment enrollment = new Enrollment();
            enrollment.StudentID = student.ID;

            EnrollmentsCreateDataModel model = new EnrollmentsCreateDataModel();

            model.Student = student;
            model.Courses = courses;
            model.Enrollment = enrollment;

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(IFormCollection formCollection)
        {
			if (ModelState.IsValid)
			{
                int studentId = int.Parse(formCollection["Enrollment.StudentID"]);
                string courseTitle = formCollection["Enrollment.CourseID"];

                Course course = _context.Courses.FirstOrDefault(c => c.Title.Equals(courseTitle));



                if (course == null) return NotFound();

                Enrollment newEnrollment = new Enrollment();

                bool exists = _context.Enrollments.Where(s => s.CourseID == course.ID && s.StudentID == studentId).Count() > 0 ? true : false;

                /*
                if(_context.Enrollments.Where(s => s.CourseID == course.ID && s.StudentID == studentId).Count() > 0)
				{
                    exists = true;
                } else
				{
                    exists = false;
				}
                */

                /*
                List<Enrollment> enrollments = _context.Enrollments.ToList();
                foreach(var enrollment in enrollments)
				{
                    if(enrollment.StudentID == studentId && enrollment.CourseID == course.ID)
					{
                        exists = true;
					}
				}*/

                if (exists)
				{
                    return BadRequest();
				}

                newEnrollment.StudentID = studentId;
                newEnrollment.CourseID = course.ID;

                _context.Enrollments.Add(newEnrollment);
                _context.SaveChanges();
			}

            return RedirectToAction(nameof(Index));
        }
    }
}
