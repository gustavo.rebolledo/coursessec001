﻿using System.Collections.Generic;

namespace CoursesSec001.Models.DataModels
{
    public class EnrollmentsCreateDataModel
    {
        public List<Course> Courses { get; set; }
        public Student Student { get; set; }
        public Enrollment Enrollment { get; set; }
    }
}
