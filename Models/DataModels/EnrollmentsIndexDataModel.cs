﻿using System.Collections.Generic;
namespace CoursesSec001.Models.DataModels
{
    public class EnrollmentsIndexDataModel
    {
        public List<Enrollment> Enrollments { get; set; }
    }
}
