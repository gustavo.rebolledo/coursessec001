﻿using System.Collections.Generic;

namespace CoursesSec001.Models.DataModels
{
    public class StudentIndexDataModel
    {
        public List<Student> Students { get; set; }
    }
}
