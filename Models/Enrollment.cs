﻿using System.ComponentModel.DataAnnotations;
namespace CoursesSec001.Models
{
    public class Enrollment
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int CourseID { get; set; }
        [Required]
        public int StudentID { get; set; }
        public byte? Grade { get; set; }

        public Course Course { get; set; }
        public Student Student { get; set; }
    }
}
