﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace CoursesSec001.Models
{
    public class Student
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        public DateTime? EnrollmentDate { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }

        public string FullName()
        {
            return FirstName + " " + LastName;
        }
    }
}
