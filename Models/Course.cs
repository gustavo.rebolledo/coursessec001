﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
namespace CoursesSec001.Models
{
    public class Course
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        [Required]
        public short Credits { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}
